---
title: Donderdag 14 september
date: 2017-09-14
---

Vandaag had ik mijn eerste keuzevak les. Heel vroeg opstaan maar het was leuk en interessant om te doen. We moesten een aantal foto's aanpassen met photoshop. 

Hierna had ik design theorie. We gingen nog wat oefenen met de stof van de hoorcollege van afgelopen dinsdag. Ik had hier nog wel wat moeite mee dus dit moet ik nog een keer gaan bekijken voor mezelf.

Ik was hierna eigenlijk uit, maar we hadden afgesproken om onze deliverables samen af te maken. Dit is goed gekomen en ik heb het ook meteen maar ingeleverd. 