---
Title: Dinsdag 5 september
Date: 2017-09-05
---

Vandaag had ik alleen maar een hoorcollege over de inhoud van design. Ik heb veel nieuwe dingen gehoord vandaag, zoals dat design meerdere betekenissen heeft (‘’design is to design a design for a design’’). Ook dat design geen kunst is, maar vooral het proces van een design. Ik vond dit een zeer nuttige college, omdat ik veel nieuwe dingen hoorde.



> Written with [StackEdit](https://stackedit.io/).