---
Title: Maandag 4 september
Date: 2017-09-04
---


Eerste studiodag ooit gehad vandaag. Het was ook wat langer dan normaal, omdat de workshop uitviel gingen we wat langer door. Ik heb met mijn groepje goed lopen brainstormen over het spel en onderzoek gedaan naar bepaalde studies en Rotterdam. Ook hebben we een kleine planning gemaakt (onderzoek woensdag af, woensdag interview studie bouwkunde, schetsen maandag af). Ik heb in mijn dummy ook wat geschets over ons concept. Ik vond het een lange dag, maar het was wel goed om zo de tijd te kunnen nemen om met je groepje te brainstormen.


> Written with [StackEdit](https://stackedit.io/).


> Written with [StackEdit](https://stackedit.io/).