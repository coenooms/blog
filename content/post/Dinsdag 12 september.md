---
Title: Dinsdag 12 september
Date: 2017-09-12
---

Vandaag had ik weer een hoorcollege, wat over verbeelding ging. Sommige dingen vond ik moeilijk te begrijpen en er kwamen ook best wel veel begrippen aan bod, dus ik ga de presentatie denk ik nog maar een keer terugkijken. Ik vond het wel erg interessant en het werd ook wel leuk verteld aan de hand van veel voorbeelden (foto's, logo's en websites).

Na de hoorcollege was ik eigenlijk vrij, maar ik had met mijn groepje afgesproken om nog wat tijd aan ons paper-prototype te besteden. Ik heb nog wat uitgeprint en overlegd met de rest.