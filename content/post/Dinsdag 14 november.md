---
Title: Dinsdag 14 november
Date: 2017-11-14
---

Om 13:00 werd ik verwacht in het Paard, Den Haag, om het tweede kwartaal te starten. In de grote zaal van het Paard werd ons de opdracht uitgelegd door de opdrachtgever ''Fabrique''. Het Paard is een club dat heel veel verschillende soorten events organiseert, maar het probleem dat bezoekers niet vaak terugkomen heerst. Ons doel is dus om iets te bedenken waardoor mensen duidelijk te zien krijgen wat er in het Paard te beleven is en een manier te bedenken hoe we de aandacht kunnen trekken. Na dit praatje was er een korte pauze, waarna we een film gingen kijken over design thinking ging. Het begin van deze documentaire was interessant, maar ik vond het op een gegeven moment overbodig worden.


> Written with [StackEdit](https://stackedit.io/).