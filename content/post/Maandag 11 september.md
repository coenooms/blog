---
Title: Maandag 11 september
Date: 2017-09-11
---

Ik ben vandaag na een paar dagen ziek zijn weer naar school gegaan. Ik begon weer in de studio waar ik met mijn groepje een standup moest doen over ons project tot nu toe. We hebben dus even verteld hoe ver we zijn. Het bleek dat we goed bezig waren dus dat was fijn om te horen. Daarna zijn we begonnen met het bedenken/schetsen van onze paper prototype. We gaan waarschijnlijk gewoon de schermpjes op a4 maken en het daarmee presenteren. 

Na de lunch had ik mijn eerste workshop over bloggen. Ik moest een site aanmaken en daarop mijn blogs posten. Het duurde best wel een tijd om door te krijgen hoe het werkt, maar het posten van de blogs is gelukt. Nu moet ik alleen nog een keer proberen om alles netjes te maken.

Ik vond het leuk om aan de website bezig te zijn ondanks alle probleempjes. Ook hebben we door het schetsen ons concept nog een stukje beter kunnen uitwerken.

