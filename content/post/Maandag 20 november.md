---
Title: Maandag 20 november
Date: 2017-11-20
---

Voor het eerst sinds een lange tijd had ik weer een normale studiodag waar ik aan de design challenge ging werken. 's ochtends hadden we een korte briefing, waarna Brian en ik aan de deliverable debriefing gingen werken. Hierin hebben we duidelijk gemaakt wie de opdrachtgevers zijn, wat het probleem is, hoe we dit gaan oplossen en wat de opdrachtgevers graag willen zien. Het was best wel nuttig om dit op een rijtje te zetten, want veel dingen werden nu duidelijk voor mij. De oorsprong van het probleem was bijvoorbeeld nog niet onderzocht, maar dat heb ik nu duidelijk in de debriefing geplaatst. 'S middags hebben we de debriefing afgerond en met de rest van het groepje een planning gemaakt voor de komende twee weken. In de resterende tijd hebben we bepaald hoe en wat we gaan onderzoeken en we zijn hiermee gestart.


> Written with [StackEdit](https://stackedit.io/).