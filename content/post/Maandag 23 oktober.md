---
title: Maandag 23 oktober
date: 2017-10-23
---

De laatste dag voor onze allerlaatste presentatie. We hebben aan ons derde prototype gewerkt en voorwerpen voor op onze presentatietafel. Ik heb de spelregels voorbereid voor de presentatie en de rest geholpen met hun taken.


> Written with [StackEdit](https://stackedit.io/).