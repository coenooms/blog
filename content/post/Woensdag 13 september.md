---
Title: Woensdag 13 september
Date: 2017-09-13
---

Weer een studiodag gehad vandaag. We hebben 's ochtends een spelanalyse gedaan. We hebben mensen ons prototype laten zien en gevraagd om hun mening. Verder hebben we wat tijd besteed aan onze deliverables. 

Na de pauze heb ik een workshop moodboard maken gevolgd. Hier heb ik meteen aan mijn moodboard over de doelgroep van mijn opdracht gewerkt. Toen de workshop voorbij was hadden we nog een studiecoach moment, waarin we nog wat dingen bespraken.