---
Title: Woensdag 15 november
Date: 2017-11-15
---

Ik heb vanochtend mijn nieuwe groepje ontmoet en met hen kennis gemaakt. We hebben wat wist je datjes geschreven op post-its en zo dus een klein beetje over jezelf verteld. Hierna hebben we samen een SWOT geschreven. Na de lange pauze, omdat er geen workshops waren, heb ik met de hele studio ''even'' nagepraat over het verloop van het eerste kwartaal. Ik kreeg mijn doelgroep toegewezen en heb met mijn team even mails uitgewisseld voor de drive zodat we volgende keer meteen kunnen beginnen. Ook zijn we begonnen met een kleine planning op Trello.


> Written with [StackEdit](https://stackedit.io/).