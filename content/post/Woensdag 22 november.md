---
Title: Woensdag 22 november
Date: 2017-11-22
---

Gisteren had ik weer eens een hoorcollege. Dit ging over; wat beïnvloed de mens? Dit was interessant, maar best wel veel en ingewikkeld. Bij de werkcollege hoop ik dat alles wat duidelijker word. 

Vandaag was het een rustige studiodag. Ik heb aan mijn oefen starrt gewerkt over de debriefing. We kregen extra informatie/vragen over elk punt van de starrt. Dit is wel erg fijn, want het is meteen een stuk duidelijker. Ik heb nu meteen een starrt over debriefing voor de competentie inrichten van het ontwerpproces. Dit beroepsproduct ga ik aanstaande maandag waarschijnlijk ook valideren.
'S middags heb ik met mijn groepje wat concepten uitgewisseld en een vragenlijst samen gesteld voor onze doelgroep. 


> Written with [StackEdit](https://stackedit.io/).