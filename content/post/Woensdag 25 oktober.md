---
title: Woensdag 25 oktober
date: 2017-10-25
---

De laatste presentatie desgin challenge 1!! 's ochtends had ik afgesproken om de laatste dingen gereed te maken voor onze pitch en onze presentatie tafel. Om half 11 kon iedereen rondlopen en elkaars werk bekijken. Ik heb een paar korte pitches gegeven en ben daarna zelf nog langs de meeste tafels gelopen. Er waren veel goede ontwerpen en ik heb er op een aantal feedback op gegeven. Na het opruimen van alles heb ik nog wat uitleg gekregen over een aantal dingen voor mijn leerdossier en heb ik met mijn groepje de laatste deliverables ingeleverd.


> Written with [StackEdit](https://stackedit.io/).